import styled from 'styled-components'
import'../../styles/colors.css'

export const Container = styled.div`
    position:relative;
    max-width:1080px;
    height:830px;
    background:var(--white);
    margin:0 auto;
    display:flex;
    flex-direction:column;
    gap:40px;
    border-radius:10px;
    h1{
        display:block;
        font-family:'Montserrat', sans-serif;
        font-weight:bold;
        font-size:35px;
        color:var(--green-regular);
        text-transform:uppercase;
        padding-top:80px;
        margin:0 auto;
    }
    p{
        display:block;
        max-width:750px;
        min-width:200px;
        margin:0 auto;
        font-size:20px;
        color:var(--black-light);        
        text-align:left;
    }
    form{
        max-width:530px;
        min-width:200px;
        margin:0 auto;
        input{
            width:248px;
            height:48px;
            border:1px solid var(--black-light);
            margin-bottom:40px;
            text-indent:15px;
            font-size:18px;
            &::placeholder{
                color:var(--black-light);
            }
        }
        .nome{
            margin-right:15px;
        }
        .email{
            margin-left:15px;
        }
        textarea{
            font-size:18px;
            width:calc(100% - 2px);
            height:197px;
            bottom:48px;
            margin-bottom:48px;
            text-indent:15px;
            padding-top:15px;
            &::placeholder{
                color:var(--black-light);
            }        
        }
        button{
            width:250px;
            height:48px;
            font-size:18px;
            color:var(--white);
            background:var(--green-regular);
            text-transform:uppercase;
            border:none;
            &:hover{
                background:var(--green-bold);
            }
        }
    }
    @media only screen and (max-width:1080px) and (min-width:581px){
        margin:0 20px;
        p{
            margin:0 auto;
            padding:0 50px;
        }
    }
    @media only screen and (max-width:580px){
        width:calc(100% - 20px);
        height:630px;
        gap:20px;
        h1{
            font-size:22px;
            padding-top:40px;
        }   
        p{
            margin:0 20px;
            font-size:14px;
        }
        form{
            margin:0 10px;
            input{
                width:calc(100% - 2px);
                margin:0 0 10px !important;
                font-size:14px;
            }
            textarea{
                width:calc(100% - 2px);
                height:110px;
                font-size:14px;
                margin-bottom:30px;
            }
            button{
                width:100%;
            }
        }
    }
`