import React from 'react';
import styled from 'styled-components'
import'../../styles/colors.css'
import LogoSuperGiantGames from '../../assets/Imagem-3.png'

const Bar = styled.div`
    position:fixed;
    top:0;
    left:0;
    z-index:1;
    width:100%;
    height:109px;
    background:var(--black-light);
    display:flex;
    align-items:center;
    justify-content:center;
    gap:30px;
    span{
        font-family:'Montserrat', sans-serif;
        font-weight:bold;
        font-size:22px;
        text-transform:uppercase;
        color:var(--white);
    }
    img{
        height:100%;
    }
    @media only screen and (max-width:560px){
        height:70px !important;
        gap:15px;
        span{
            font-size:0.8rem
        }
    }
`
function TopBar() {
    return  <Bar>
                <img src={LogoSuperGiantGames} alt="Logo da Super Giant Games"/>
                <span>supergiantgames</span>
            </Bar>
}

export default TopBar;