import styled from 'styled-components'
import'../../styles/colors.css'

export const Container = styled.div`
    max-width:1500px;
    margin:0 auto;
    button.rec-dot{
        background-color: var(--black-light);
    }   
    button.rec-dot:hover, button.rec-dot:active, button.rec-dot:focus  {
        box-shadow: 0 0 1px 3px var(--white);
    }
    .bt-slide{
        background:none;
        border:none;
        svg{
            font-size:54px;
            color:var(--white);
            cursor: pointer;
            &:hover{
                color:var(--baby-blue);
            }
        }
    }
    @media only screen and (max-width:560px){
        .bt-slide{
            background:none;
            svg{
                display:none;
            }
        }
    }
`