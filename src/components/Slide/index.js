import React, { useState } from 'react';
import Carousel, { consts } from 'react-elastic-carousel'
import { Container } from './styles';

import { BsChevronLeft, BsChevronRight } from 'react-icons/bs'

import CardSlide from '../../elements/CardSlide'

import GrantImage from '../../assets/Grant.png'
import RedImage from '../../assets/Red.png'
import SybilImage from '../../assets/Sybil.png'

function Slide() {
    const [index, setIndex] = useState(3)    
    const [dot, setDot] = useState(false);
    function handlerWindowSize(){
        const width = window.innerWidth;
        if(width < 1330 && width > 935){
            setIndex(2)
        }else if(width < 935){
            setIndex(1)
        }else{
            setIndex(3)
        }
        handlerPagination()
    }
    function handlerPagination(){
        const width = window.innerWidth;
        if(width <= 560){
            setDot(true)
        }else{
            setDot(false)
        }
    }
    function myArrow({ type, onClick, isEdge }) {
        const pointer = type === consts.PREV ?<BsChevronLeft /> : <BsChevronRight />
        return (
          <button className="bt-slide" onClick={onClick} disabled={isEdge}>
            {pointer}
          </button>
        )
    }
    window.addEventListener('resize', handlerWindowSize)
    window.addEventListener('pageshow', handlerWindowSize)
    return  <Container>
                <Carousel 
                    itemsToShow={index}
                    pagination={dot}
                    renderArrow={myArrow}
                    >
                    <CardSlide 
                        img={GrantImage}
                        text="A Camerata foi apenas os dois no início, e suas 
                        fileiras nunca foram destinadas a exceder um número a 
                        ser contado em uma mão."
                    />
                    <CardSlide 
                        img={RedImage}
                        text="Red, uma jovem cantora, entrou em posse do Transistor. 
                        Sendo a poderosa espada falante. O grupo Possessores quer 
                        tanto ela quanto o Transistor e está perseguindo 
                        implacavelmente a sua procura." 
                    />
                    <CardSlide 
                        img={SybilImage}
                        text="Sybil é descrita pelo Transistor como sendo os &quot;olhos e ouvidos&quot; da Camerata."
                    />
                    <CardSlide 
                        img={GrantImage}
                        text="A Camerata foi apenas os dois no início, e suas 
                        fileiras nunca foram destinadas a exceder um número a 
                        ser contado em uma mão."
                    />
                    <CardSlide 
                        img={RedImage}
                        text="Red, uma jovem cantora, entrou em posse do Transistor. 
                        Sendo a poderosa espada falante. O grupo Possessores quer 
                        tanto ela quanto o Transistor e está perseguindo 
                        implacavelmente a sua procura." 
                    />
                    <CardSlide 
                        img={SybilImage}
                        text="Sybil é descrita pelo Transistor como sendo os &quot;olhos e ouvidos&quot; da Camerata."
                    />
                </Carousel>
            </Container>
}

export default Slide;