import styled from 'styled-components'
import ImgBackground from '../../assets/Imagem-2.png';
export const Container = styled.div`
    width:100%;
    height:auto;
`
export const Introduction = styled.div`
    width:100%;
    background:url(${ImgBackground});
    background-size:cover;
    background-position:center;
    overflow:hidden;
    .scroll-button{
        position:absolute;
        top:1050px;
        left:50%;
        svg{
            display:block;
            font-size:1.8rem;
            margin:0 2px;
            color:var(--white);
        }
        > div{
            width:29px;
            height:48px;
            background:var(--white);
            border-radius:15px;
            /* nescessario para margem da div filho funcionar */
            border:1px solid white;
            div{
                width:4px;
                height:9px;
                border-radius:5px;
                margin:8px auto;
                background:var(--black);
            }
        }
    }
    @media only screen and (max-width:768px){
        .scroll-button{
            top:570px;
            svg{
                font-size:1.2rem;
            }
            > div{
                width:20px;
                height:35px;
            }
        }
    }
`
export const Characters = styled.div`
    width:100%;
    padding:100px 0;
    @media only screen and (max-width:768px){
        padding:50px 0;
    }
`
export const Contact = styled.div`
    position:relative;
    width:100%;
    padding:50px 0;
    &::before{
        position:absolute;
        display:block;
        content:"";
        top:50%;
        left:0;
        width:100%;
        height:80%;
        background: transparent linear-gradient(143deg, #7DEDE2 0%, #58B790 100%) 0% 0% no-repeat padding-box;
        transform:translateY(-50%);
    }
`
export const Footer = styled.footer`
    width:100%;
    height:300px;
    @media only screen and (max-width:768px){
        height:120px;
    }
`