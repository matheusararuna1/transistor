import React from 'react';

import { Container, Introduction, Characters, Contact, Footer } from './styles';

import { MdKeyboardArrowDown } from 'react-icons/md'

//COMPONENTS
import TopBar from '../../components/TopBar'
import Slide from '../../components/Slide'
import Form from '../../components/Form'

//ELEMENTS
import CardMain from '../../elements/CardMain'
import BtBackTop from '../../elements/BtBackTop';

function Main() {
  return    <>
                <Container>
                    <TopBar />
                    <Introduction>
                        <CardMain />
                        <div className="scroll-button">
                            <div>
                                <div />
                            </div>
                            <MdKeyboardArrowDown />
                        </div>
                    </Introduction>
                    <Characters>
                        <Slide />
                    </Characters>
                    <Contact>
                        <Form />
                    </Contact>
                    <Footer />
                    <BtBackTop />
                </Container>
            </>;
}

export default Main;