import Main from './pages/Main'
import'./styles/colors.css'

function App() {
  return (
    <div style={{ background:'var(--black-light)'}}>
      <Main />
    </div>
  );
}

export default App;
 