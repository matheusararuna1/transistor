import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
    *{
        margin:0;
        padding:0;
        font-family:'Open Sans', sans-serif;
    }
    button{
        cursor: pointer;
    }
    input, button, textarea{
        outline:none;
    }
    a{
        font-style:inherit;
    }
`
export default GlobalStyle