import styled from 'styled-components'
import'../../styles/colors.css'

export const Card = styled.div`
    position:relative;
    width:387px;
    height:820px;
    flex:none;
    img{
        position:absolute;
        top:0;
        left:0;
        width:100%;
    }    
    .rectangle-short{
        position:absolute;
        width:calc(100% - 28px);
        height:418px;
        top:97px;
        left:10px;
        border-radius:90px;
        background:var(--black-light);
        border:4px solid var(--white);
    }
    .rectangle-large{
        position:absolute;
        top:200px;
        width:100%;
        height:613px;
        background:white;
        border-radius:27px;
        box-shadow: 0px 3px 6px #FFFFFF63;
    }
    .character-text{
        position:absolute;
        top:545px;
        display:block;
        color:var(--black-light);
        font-size:20px;
        font-weight:regular;
        text-align:left;
        margin:0 30px;
    }
    @media only screen and (max-width:780px){
        width:280px;
        height:550px;
        img{
            width:100%;
        }
        .rectangle-short{
            top:65px;
            height:305px;
            border-radius:60px;
        }
        .rectangle-large{
            top:150px;
            height:395px;
            border-radius:15px;
        }
        .character-text{
            top:390px;
            font-size:14px;
        }        
    }
`