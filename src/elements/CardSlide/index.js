import React from 'react';

import { Card } from './styles';

function CardSlide({ img, text }) {
  return <Card>
            <div className="rectangle-large" />
            <div className="rectangle-short" />
            <div className="rectangle-white" />
            <img className="image" src={ img } alt=""/>
            <span className="character-text">{ text }</span>
        </Card>;
}

export default CardSlide;