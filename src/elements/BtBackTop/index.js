import React, { useState, useRef } from 'react';
import styled from 'styled-components'

import { BsChevronUp } from 'react-icons/bs'

const Button = styled.button`
    position:fixed;
    bottom:100px;
    right:100px;
    width:107px;
    height:107px;
    border-radius:50%;
    display:none;
    align-items:center;
    justify-content:center;
    border:none;
    background:var(--white);
    svg{
        font-size:2rem;
        color:var(--black-light);
    }
    &:hover{
        background:var(--gray-light);
        svg{
            color:var(--white);
        }
    }
    &.show{
        display:flex;
    }
    @media only screen and (max-width:1024px){
        width:70px;
        height:70px;
        bottom:50px;
        right:50px;

        svg{
            font-size:1.5rem;
        }
    }
    @media only screen and (max-width:520px){
        width:40px;
        height:40px;
        bottom:30px;
        right:30px;
        svg{
            font-size:1rem;
        }
    }
`

function BtBackTop() {
    const refBt = useRef();
    const [showScroll, setShowScroll] = useState(true);
    function checkScroll(e){
        if(window.pageYOffset > 400){
            setShowScroll(true);
        }else if(window.pageYOffset <= 400){
            setShowScroll(false);
        }
    }
    function handleScroll(){
        window.scrollTo({top: 0, behavior: 'smooth'})
    }
    window.addEventListener('scroll', checkScroll)
    return  <Button ref={refBt} onClick={handleScroll} className={ showScroll ? "show" : ""}>
                <BsChevronUp />
            </Button>;
}

export default BtBackTop;