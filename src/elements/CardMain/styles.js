import styled from 'styled-components'
import BackCard from '../../assets/Imagem-card.svg'
import'../../styles/colors.css'


export const Card = styled.div`
    position:relative;
    width:586px;
    height:1080px;
    margin:0 auto;
    background:url(${BackCard});
    background-size:auto 100%;
    background-repeat:no-repeat; 
    span{
        position:absolute;
        top: 121px;
        left:30px;
        font-size:15px;
        font-weight:bold;
        text-transform:uppercase;
        color:var(--white);
    }
    p{
        position:absolute;
        top:945px;
        left:50%;
        transform:translateX(-50%);
        display:block;
        width:271px;
        height:121px;
        font-size:20px;
        font-weight:regular;
        text-align:center;
        color:var(--white);
    }
    > img{
        position:absolute;
    }
    .front-image{
        position:absolute;
        top:163px;
        left:30px;
        width:526px;
        height:754px;
    }
    .top-left{
        top:485px;
        left:-35px;
        width:130px;
        height:119px;
    }
    .bottom-left{
        top:604px;
        left:20px;
        width:96px;
        height:100px;
        
    }
    .top-right{
        top:419px;
        right:-30px;
        width:181px;
        height:155px;
    }
    .bottom-right{
        top:586px;
        right:-85px;
        width:157px;
        height:155px;
    }
    @media only screen and (max-width:780px){
        width:300px;
        height:590px;
        span{
            font-size:10px;
            top:80px;
        }
        p{
            font-size:14px;
            width:180px;
            top:500px;
        }
        .front-image{
            width:250px;
            height:380px;
            top:105px;
        }
        .top-left{
            width:65px;
            height:50px;
            top:270px;
            left:-5px;

        }
        .bottom-left{
            width:48px;
            height:50px;
            top:330px;
            left:25px;
        }
        .top-right{
            width:80px;
            height:75px;
            top:260px;
            right:-10px;
        }
        .bottom-right{
            width:76px;
            height:75px;
            top:315px;
            right:-20px;
        }
    }
`