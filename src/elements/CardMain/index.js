import React from 'react';
import { Card } from './styles'

import FrontCard from '../../assets/Imagem-card.png'
import TopLeft from '../../assets/Top-left.png'
import BottomLeft from '../../assets/Bottom-left.png'
import TopRight from '../../assets/Top-right.png'
import BottomRight from '../../assets/Bottom-right.png'

function CardMain() {
    return  <Card>
                <span>transistor - red the singer</span>
                <img className="front-image" src={FrontCard} alt=""/>
                <p>
                    "Olha, o que quer que você esteja pensando, me faça um favor, não solte."
                </p>
                <img className="top-left" src={TopLeft} alt=""/>
                <img className="bottom-left" src={BottomLeft} alt=""/>
                <img className="top-right" src={TopRight} alt=""/>
                <img className="bottom-right" src={BottomRight } alt=""/>
            </Card>;
}

export default CardMain;