# Transistor
Teste Front-end

### Instalar
Projeto requer [Node.js](https://nodejs.org/) v12+ para ser executado.

#### Rodar o App
Clone, instale as dependencia e rode o App.
```sh
$ git clone https://matheusararuna1@bitbucket.org/matheusararuna1/transistor.git
$ cd /transistor
$ yarn install && yarn start
```
ou pelo NPM
```sh
$ npm install && npm start
```